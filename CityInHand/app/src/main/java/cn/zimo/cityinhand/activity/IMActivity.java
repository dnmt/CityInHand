package cn.zimo.cityinhand.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.jaeger.library.StatusBarUtil;
import com.just.agentweb.AgentWeb;

import cn.zimo.cityinhand.R;

/**
 * @author 子墨
 * @datetime 2018/9/22 17:10
 * @description 即时通讯
 */
public class IMActivity extends AppCompatActivity {

    AgentWeb mAgentWeb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE |
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        String url = "file:///android_asset/" + "html/right_now_chat.html";
        // 半透明任务栏
        StatusBarUtil.setTranslucent(this);
        LinearLayout linearLayout = findViewById(R.id.ll_root);
        // 加载webview控件
        mAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(linearLayout, new LinearLayout.LayoutParams(-1, -1))
                .useDefaultIndicator()
                .createAgentWeb()
                .ready()
                .go(url);
    }

    // 返回键
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (mAgentWeb.handleKeyEvent(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
