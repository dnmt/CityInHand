package cn.zimo.cityinhand.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.widget.TextView;

/**
 * Created by 方少 on 2018/6/27.
 * QQ：1378501215
 * Email：fangjunjiefjj@163.com
 */
public class Isinternet {
    public static boolean isNetworkAvalible(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            return false;
        } else {
            NetworkInfo[] net_inf = connectivityManager.getAllNetworkInfo();
            if(net_inf != null){
                for(int i = 0;i < net_inf.length;i++){
                    if(net_inf[i].getState() == NetworkInfo.State.CONNECTED)
                        return true;
                }
            }

        }
        return false;
    }

    public static void checkNetwork(final Activity activity){
        if (!isNetworkAvalible(activity)){
            TextView msg = new TextView(activity);
            msg.setText("当前没有可以使用的网络，请设置网络！");
            msg.setTextColor(Color.WHITE);
            msg.setTextSize(20);
            new AlertDialog.Builder(activity)
                    .setTitle("网络状态提示")
                    .setView(msg)
                    .setPositiveButton("确定",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    activity.startActivityForResult(new Intent(Settings.ACTION_WIRELESS_SETTINGS),0);
                                }
                                }).create().show();
            }
            return;

    }
}
