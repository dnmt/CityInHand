package cn.zimo.cityinhand;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.LinearLayout;

import com.jaeger.library.StatusBarUtil;
import com.just.agentweb.AgentWeb;

import cn.zimo.cityinhand.officalnotice.OfficalNotice;

public class MainActivity extends AppCompatActivity {

    AgentWeb mAgentWeb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_UP){
            Intent intent = new Intent(this,OfficalNotice.class);
            startActivity(intent);
            this.overridePendingTransition(R.anim.activity_fade_in,R.anim.activity_fade_out);
        }

        return true;
    }
}
        /*
        String url = "file:///android_asset/" + "html/index.html";
        // 半透明任务栏
        StatusBarUtil.setTranslucent(this);
        LinearLayout linearLayout = findViewById(R.id.ll_root);
        // 加载webview控件
        mAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(linearLayout, new LinearLayout.LayoutParams(-1, -1))
                .useDefaultIndicator()
                .createAgentWeb()
                .ready()
                .go(url);
    }



    // 返回键
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (mAgentWeb.handleKeyEvent(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }*/
