package cn.zimo.cityinhand.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;

import cn.zimo.cityinhand.R;
import cn.zimo.cityinhand.activity.LoginActivity;
import cn.zimo.cityinhand.config.HttpConfig;
import cn.zimo.cityinhand.costumewidget.PersonalItemView;
import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

import static android.content.Context.MODE_PRIVATE;

/**
 * @author 子墨
 * @datetime 2018/9/22 9:38
 * @description 个人碎片
 */
public class MyFragment extends Fragment implements View.OnClickListener{

    // 头像背景
    private ImageView iv_head_background;
    // 圆形头像
    private ImageView iv_circle_head;
    // 分割线
    private ImageView iv_split_line;
    // 图像中间的用户名
    private TextView tv_user_name;
    private TextView tv_user_tel;

    private Button btn_logout;

    // 上下文
    private Activity context;

    // item——昵称
    private PersonalItemView item_nick_name;
    private PersonalItemView item_sex;
    private PersonalItemView item_position;
    private PersonalItemView item_tel;
    private PersonalItemView item_details;
    private PersonalItemView item_changepwd;
    private PersonalItemView item_about_us;

    public MyFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my, container, false);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        context = getActivity();
        super.onActivityCreated(savedInstanceState);
        initViews(context);

        Glide.with(context).load(R.drawable.head)
                .bitmapTransform(new BlurTransformation(context, 25), new CenterCrop(context))
                .into(iv_head_background);

        Glide.with(this).load(R.drawable.head)
                .bitmapTransform(new CropCircleTransformation(context))
                .into(iv_circle_head);
    }

    /**
     * 初始化控件
     */
    private void initViews(Activity context) {

        iv_head_background = context.findViewById(R.id.iv_head_background);
        iv_circle_head = context.findViewById(R.id.iv_circle_head);
        iv_split_line = context.findViewById(R.id.iv_split_line);

        tv_user_name = context.findViewById(R.id.tv_user_name);
        tv_user_tel = context.findViewById(R.id.tv_user_tel);

        item_nick_name = context.findViewById(R.id.item_nick_name);
        item_sex = context.findViewById(R.id.item_sex);
        item_position = context.findViewById(R.id.item_position);

        item_tel = context.findViewById(R.id.item_tel);
        item_details = context.findViewById(R.id.item_details);
        item_changepwd = context.findViewById(R.id.item_changepwd);

        item_about_us = context.findViewById(R.id.item_about_us);
        btn_logout = context.findViewById(R.id.btn_logout);

        setListener();
    }

    /**
     * 设置监听
     */
    private void setListener() {

        iv_circle_head.setOnClickListener(this);

        item_nick_name.setOnClickListener(this);
        item_sex.setOnClickListener(this);
        item_position.setOnClickListener(this);

        item_tel.setOnClickListener(this);
        item_details.setOnClickListener(this);
        item_changepwd.setOnClickListener(this);

        item_about_us.setOnClickListener(this);
        btn_logout.setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {
        int id = v.getId();
        String content = null;
        switch (id){
            case R.id.iv_circle_head:

                break;
            case R.id.item_nick_name:
                content = "昵称";
                break;
            case R.id.item_sex:
                content = "性别";
                break;
            case R.id.item_position:
                content = "职位";
                break;
            case R.id.item_tel:
                content = "电话";
                break;
            case R.id.item_details:
                content = "个人详情";
                break;
            case R.id.item_changepwd:
                content = "修改密码";
                break;
            case R.id.item_about_us:
                content = "关于我们";
                break;
            case R.id.btn_logout:
                SharedPreferences sharedPreferences = context.getSharedPreferences("login",MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                HttpConfig.Sign = null;
                HttpConfig.SessionId = null;
                editor.putString("sessionid",HttpConfig.SessionId);
                editor.putString("sign",HttpConfig.Sign);
                editor.putLong("loginTime",0);
                editor.commit();
                Toast.makeText(context,"已退出",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context,LoginActivity.class);
                startActivity(intent);
                return;

        }
//        content = "修改" + content + "接口尚未开通，请耐心等待";
        Toast.makeText(context,content,Toast.LENGTH_SHORT).show();
    }

}
