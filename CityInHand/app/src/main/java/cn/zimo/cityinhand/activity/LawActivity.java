package cn.zimo.cityinhand.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jaeger.library.StatusBarUtil;
import com.just.agentweb.AgentWeb;

import java.util.HashMap;
import java.util.Map;

import cn.zimo.cityinhand.R;
import cn.zimo.cityinhand.utils.HttpUtils;

public class LawActivity extends AppCompatActivity {

    AgentWeb mAgentWeb;
    private Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_law);
        // 半透明任务栏
        StatusBarUtil.setTranslucent(this);
        final LinearLayout linearLayout = findViewById(R.id.ll_root);

        context = this;
        new Thread(){
            @Override
            public void run() {
                Map<String,String> params = new HashMap<>();
                params.put("username","admin");
                params.put("password","admin");
                final String json = HttpUtils.submitPostData("http://law.sipingtai.top/login/login-check",params,"utf-8");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (json == null || json.startsWith("err")){
                            Toast.makeText(context,"网络出现错误",Toast.LENGTH_SHORT).show();
                            context.finish();
                            return;
                        }
                        JSONObject object = (JSONObject) JSON.parse(json);
                        final String auth = object.getString("data");
                        String url = "http://law.sipingtai.top/app/service/index.html?auth="+auth;
                        // 加载webview控件
                        mAgentWeb = AgentWeb.with(context)
                                .setAgentWebParent(linearLayout, new LinearLayout.LayoutParams(-1, -1))
                                .useDefaultIndicator()
                                .createAgentWeb()
                                .ready()
                                .go(url);
                    }
                });
            }
        }.start();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (mAgentWeb.handleKeyEvent(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
