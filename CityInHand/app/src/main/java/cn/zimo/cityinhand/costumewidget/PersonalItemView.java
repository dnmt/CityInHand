package cn.zimo.cityinhand.costumewidget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.zimo.cityinhand.R;


/**
 * @author 子墨
 * @datetime 2018/7/18 9:55
 * @description 自定义个人信息条目控件
 */
public class PersonalItemView extends LinearLayout {

    // id标识
    private int id;
    // 是否显示底部的下划线
    private boolean isShowBottomLine = true;
    // 是否显示左侧的图标
    private boolean isShowLeftIcon = true;
    // 是否显示右侧的箭头
    private boolean isShowRightArrow = true;
    // 是否展示右侧的文本
    private boolean isShowRightDesc = true;

    // 列表左侧的图标
    private ImageView leftIcon;
    // 左侧的标题
    private TextView leftTitle;
    // 右侧的描述
    private EditText rightDesc;
    // 右侧的箭头
    private ImageView rightArrow;
    // 底部下划线图标
    private ImageView bottomLine;
    // 一个整体item的view
    private RelativeLayout rootView;

    public PersonalItemView(Context context) {
        this(context, null);
    }

    public PersonalItemView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PersonalItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {

        super(context, attrs, defStyleAttr);
        // 添加布局文件
        View view = LayoutInflater.from(context).inflate(R.layout.personal_item_view_layout, null);
        addView(view);
        // 获取自定义控件参数
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.PersonalItemView);
        // 找到控件
        leftIcon = findViewById(R.id.left_icon);
        leftTitle = findViewById(R.id.left_title);
        rightDesc = findViewById(R.id.right_desc);
        rightArrow = findViewById(R.id.right_arrow);
        bottomLine = findViewById(R.id.bottom_line);
        rootView= findViewById(R.id.root_item);
        // 设置控件属性
        isShowBottomLine = ta.getBoolean(R.styleable.PersonalItemView_show_bottom_line, true);// 得到是否显示底部下划线属性值
        isShowLeftIcon = ta.getBoolean(R.styleable.PersonalItemView_show_left_icon, true);// 得到是否显示左侧图标属性值
        isShowRightArrow = ta.getBoolean(R.styleable.PersonalItemView_show_right_arrow, true);// 得到是否显示右侧图标属性值
        isShowRightDesc = ta.getBoolean(R.styleable.PersonalItemView_show_right_desc,true);// 得到是否显示右侧描述的属性值

        leftIcon.setBackground(ta.getDrawable(R.styleable.PersonalItemView_left_icon));// 设置左侧图标
        leftIcon.setVisibility(isShowLeftIcon ? View.VISIBLE : View.INVISIBLE);// 设置左侧箭头图标是否显示

        leftTitle.setText(ta.getString(R.styleable.PersonalItemView_left_text));// 设置左侧标题文字
        rightDesc.setText(ta.getString(R.styleable.PersonalItemView_right_text));// 设置右侧文字描述
        rightDesc.setVisibility(isShowRightDesc ? View.VISIBLE : View.INVISIBLE);// 设置右侧文字描述是否显示

        rightArrow.setVisibility(isShowRightArrow ? View.VISIBLE : View.INVISIBLE);// 设置右侧箭头图标是否显示
        bottomLine.setVisibility(isShowBottomLine ? View.VISIBLE : View.INVISIBLE);// 设置底部图标是否显示

        // 回收
        ta.recycle();
    }


    /**
     * 设置左侧图标资源
     * @param sourceid
     */
    public void setLeftIcon(int sourceid) {
        Drawable drawable=getResources().getDrawable(sourceid);
        leftIcon.setBackground(drawable);
    }

    /**
     * 设置左侧标题文字内容
     * @param value
     */
    public void setLeftTitle(String value) {
        leftTitle.setText(value);
    }

    /**
     * 设置右侧描述文字内容
     * @param value
     */
    public void setRightDesc(String value) {
        rightDesc.setText(value);
    }
    /**
     * 设置右侧箭头是否显示
     * @param value
     */
    public void setShowRightArrow(boolean value) {
        rightArrow.setVisibility(value ? View.VISIBLE : View.INVISIBLE);//设置右侧箭头图标是否显示
    }

    /**
     * 设置是否显示下面的分割线
     * @param value
     */
    public void setShowBottomLine(boolean value) {
        bottomLine.setVisibility(value ? View.VISIBLE : View.INVISIBLE);//设置右侧箭头图标是否显示
    }



}
