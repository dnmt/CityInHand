package cn.zimo.cityinhand.utils;

import android.support.annotation.StringDef;

/**
 * Created by 方少 on 2018/6/27.
 * QQ：1378501215
 * Email：fangjunjiefjj@163.com
 */
@StringDef({
        MDFonts.Plus,
        MDFonts.Trash,
        MDFonts.Photo
})
public @interface MDFonts{
    String Plus = "\ue6e3";
    String Trash = "\ue6d4";
    String Photo = "\ue6cd";
}