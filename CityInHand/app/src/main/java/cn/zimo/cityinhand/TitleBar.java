package cn.zimo.cityinhand;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by 方少 on 2018/6/27.
 * QQ：1378501215
 * Email：fangjunjiefjj@163.com
 */
public class TitleBar extends Activity {
    private TextView tv_title;
    private Button btn_backforward;
    private Button btn_forward;
    public final static String sdPath = Environment.getExternalStorageDirectory().toString();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_bar);
    }

    public void initTitleBar(){
        tv_title = findViewById(R.id.tv_title);
        btn_backforward = findViewById(R.id.btn_backword);
        btn_forward = findViewById(R.id.btn_forward);
    }

    public void invisibleBackforward(){
        btn_backforward.setVisibility(View.INVISIBLE);
    }
    public void invisibleForward(){
        btn_forward.setVisibility(View.INVISIBLE);
    }

    public void setTitle(String title){
        tv_title.setText(title);
    }

    public void setBackForward(String str){
        btn_backforward.setText(str);
    }
    public void setForward(String str){
        btn_forward.setText(str);
    }

    public void onBackForwardClick(View v) {
        finish();
    }
}
