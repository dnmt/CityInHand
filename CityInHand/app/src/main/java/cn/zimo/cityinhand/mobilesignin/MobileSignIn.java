package cn.zimo.cityinhand.mobilesignin;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import cn.zimo.cityinhand.R;
import cn.zimo.cityinhand.TitleBar;
import cn.zimo.cityinhand.activity.RecordActivity;
import cn.zimo.cityinhand.utils.CameraView;
import cn.zimo.cityinhand.utils.MDFonts;
import cn.zimo.cityinhand.utils.MDFontsUtils;

public class MobileSignIn extends TitleBar {
    private Button btn_startPhoto;
    private Camera camera;
    private CameraView cameraView;
    private static final int FRONT = 1; //标记前置摄像头
    private static final int BACK = 2;  //标记后置摄像头
    private int currentCameraType = -1; //当前打开摄像头标记
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_sign_in);
        initTitleBar();
        initView();
        startCamera();
    }
    private void initView(){
        setTitle("移动考勤");
        btn_startPhoto = findViewById(R.id.btn_takephoto);
        btn_startPhoto.setText(MDFonts.Photo);
        btn_startPhoto.setTextSize(40);
        btn_startPhoto.setBackgroundColor(Color.rgb(204,204,204));
        MDFontsUtils.setOcticons(btn_startPhoto);
        setForward("操作");

    }

    //检测是否有摄像头
    private boolean checkCamera(){
        return MobileSignIn.this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }
    //开启摄像头
    private void startCamera(){
        if(!checkCamera()){
            Toast.makeText(MobileSignIn.this,"未检测到摄像头",Toast.LENGTH_SHORT).show();
            //this.finish();
        }
        camera = openCamera(FRONT);
        btn_startPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO:拍照
                camera.takePicture(null, null, new Camera.PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] bytes, Camera camera) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                        try {
                            File f = new File(sdPath + "/demoDoc/");
                            if(!f.exists()){
                                f.mkdirs();
                            }
                            FileOutputStream fileOutputStream = new FileOutputStream(sdPath + "/demoDoc/" + System.currentTimeMillis() + ".jpg");

                            bitmap.compress(Bitmap.CompressFormat.JPEG,85,fileOutputStream);
                            camera.stopPreview();
                            camera.startPreview();
                            Toast.makeText(MobileSignIn.this,"考勤成功",Toast.LENGTH_SHORT).show();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        });
        cameraView = findViewById(R.id.sv_head_photo);
        cameraView.init(camera);


    }

    @SuppressLint("NewApi")
    private Camera openCamera(int type){
        int frontIndex = -1;
        int backIndex = -1;
        int cameraCount = Camera.getNumberOfCameras();
        Camera.CameraInfo info = new Camera.CameraInfo();



        for(int cameraIndex = 0;cameraIndex < cameraCount;cameraIndex++){
            Camera.getCameraInfo(cameraIndex,info);                     //获取camera信息 判断是前置还是后置
            if(info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT){
                frontIndex = cameraIndex;
            }else if(info.facing == Camera.CameraInfo.CAMERA_FACING_BACK){
                backIndex = cameraIndex;
            }
        }
        currentCameraType = type;
        if(type == FRONT && frontIndex != -1){
            return Camera.open(frontIndex);
        }else if(type == BACK && backIndex != -1){
            return Camera.open(backIndex);
        }



        return  null;
    }

    private void changeCamera() throws IOException{
        camera.startPreview();
        camera.release();
        if(currentCameraType == FRONT){
            camera = openCamera(BACK);
        }else if(currentCameraType == BACK){
            camera = openCamera(FRONT);
        }
        camera.setDisplayOrientation(90);
        camera.setPreviewDisplay(cameraView.getHolder());
        camera.startPreview();
    }

    public void onForwardClick(View v) {

        PopupMenu popup = new PopupMenu(this, v);//第二个参数是绑定的那个view
        final MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_camera, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.change_camera:
                        try {
                            changeCamera();
                            Toast.makeText(MobileSignIn.this,"已切换",Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case R.id.record_camera:
                        Intent intent = new Intent(MobileSignIn.this, RecordActivity.class);
                        startActivity(intent);
                        break;
                }
                return false;
            }
        });
        // 绑定菜单项的点击事件
//            popup.setOnMenuItemClickListener(MobileSignIn.this);
        // 显示(这一行代码不要忘记了)
        popup.show();

    }


}
