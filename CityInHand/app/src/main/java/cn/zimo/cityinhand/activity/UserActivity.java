package cn.zimo.cityinhand.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.zimo.cityinhand.R;
import cn.zimo.cityinhand.domain.User;
import cn.zimo.cityinhand.utils.HttpUtils;

public class UserActivity extends AppCompatActivity {

    private ListView userList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        userList = findViewById(R.id.lv_user_list);
        try{
            final Map<String,String> param = new HashMap<>();
            param.put("page",1+"");
            param.put("limit",10+"");
            new Thread(){
                @Override
                public void run() {
                    final String json = HttpUtils.submitPostData("http://117.172.164.151:9999/ajax/auth/branch_person_list",param,"utf=8");
                    Log.d("json",json);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (json == null || json.startsWith("err")){
                                Toast.makeText(UserActivity.this,"网络出现错误",Toast.LENGTH_SHORT).show();
                                UserActivity.this.finish();
                                return;
                            }
                            JSONObject object = JSON.parseObject(json);
                            JSONArray data = object.getJSONArray("data");
                            final List<User> users = JSON.parseArray(data.toJSONString(),User.class);
                            userList.setAdapter(new BaseAdapter() {
                                @Override
                                public int getCount() {
                                    return users.size();
                                }

                                @Override
                                public Object getItem(int position) {
                                    return users.get(position);
                                }

                                @Override
                                public long getItemId(int position) {
                                    return users.get(position).getUserId();
                                }

                                @Override
                                public View getView(int position, View convertView, ViewGroup parent) {
                                    View view = View.inflate(UserActivity.this,android.R.layout.simple_list_item_1,null);
                                    TextView tv = view.findViewById(android.R.id.text1);
                                    User u = users.get(position);
                                    if (u.getBranchName() == null){
                                        tv.setText((position+1)+". "+u.getName()+"  <无>");
                                    }else {
                                        tv.setText((position+1)+". "+u.getName()+"  <"+u.getBranchName()+">");
                                    }

                                    return view;
                                }
                            });
                            userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Intent intent = new Intent(UserActivity.this,IMActivity.class);
                                    startActivity(intent);
                                }
                            });
                        }
                    });
                }
            }.start();
        }catch (Exception e){
            Toast.makeText(this,"出现错误",Toast.LENGTH_SHORT).show();
            this.finish();
        }
    }
}
