package cn.zimo.cityinhand.shareinformation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cn.zimo.cityinhand.R;
import cn.zimo.cityinhand.TitleBar;

public class ShareInfo extends FragmentActivity {
    private TabLayout tabLayout = null;
    private ViewPager viewPager;
    private List<String> mTitleList = Arrays.asList("消息","文件","资料");
    private List<Fragment> mFragmentList = new ArrayList<>();
    private TabFragmentPagerAdapter mTabFragmentPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_info);
        initView();
    }
    //初始化
    private void initView(){
        tabLayout = findViewById(R.id.tablayout);
        viewPager = findViewById(R.id.tab_viewPager);
        //设定title，关闭右上角选择
        TextView tv = findViewById(R.id.tv_title);
        tv.setText("信息共享");
        Button btn_forward = findViewById(R.id.btn_forward);
        btn_forward.setVisibility(View.INVISIBLE);


        mFragmentList.add(new ShareInfoMsg());
        mFragmentList.add(new ShareInfoFiles());
        mFragmentList.add(new ShareInfoDoc());

        mTabFragmentPagerAdapter = new TabFragmentPagerAdapter(getSupportFragmentManager(),mTitleList,mFragmentList);
        viewPager.setAdapter(mTabFragmentPagerAdapter);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(viewPager);


    }

    public void onBackForwardClick(View v){
        finish();
    }
    //Fragment的适配器
    class TabFragmentPagerAdapter extends FragmentPagerAdapter{

        private List<String> mTitle;
        private List<Fragment> mFragment;
        public TabFragmentPagerAdapter(FragmentManager fm, List<String> titles, List<Fragment> fragments ) {
            super(fm);
            mTitle = titles;
            mFragment = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return mFragment.get(position);
        }


        @Override
        public int getCount() {
            return mFragment.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitle.get(position);
        }
    }
}
