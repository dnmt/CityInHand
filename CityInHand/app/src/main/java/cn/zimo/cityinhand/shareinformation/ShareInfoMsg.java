package cn.zimo.cityinhand.shareinformation;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cn.zimo.cityinhand.R;

/**
 * Created by 方少 on 2018/6/27.
 * QQ：1378501215
 * Email：fangjunjiefjj@163.com
 */
public class ShareInfoMsg extends Fragment implements AbsListView.OnScrollListener {
    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Myadapter adapter;
    private List<String> demoList = new ArrayList<>();
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch(msg.what){
                case 1:
                    if(swipeRefreshLayout.isRefreshing()){
                        adapter.notifyDataSetChanged();   //信息同步
                        swipeRefreshLayout.setRefreshing(false); //停止刷新显示
                    }
                    break;
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_share_message, container, false);
        listView = rootView.findViewById(R.id.main_lv);
        swipeRefreshLayout = rootView.findViewById(R.id.main_srl);
        demoList.add("测试消息1");
        demoList.add("测试消息2");
        demoList.add("测试消息3");
        demoList.add("测试消息4");

        //设定滑动监听
        listView.setOnScrollListener(this);
        adapter = new Myadapter();
        listView.setAdapter(adapter);
        //设定颜色切换
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                android.R.color.holo_orange_light, android.R.color.holo_red_light);
        //设定刷新监听
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            //TODO：刷新动作
            @Override
            public void onRefresh() {
                new LoadDataThread().start();
            }
        });
        return rootView;
    }

    private int visibleLastIndex;
    //ListView 的滑动状态改变
    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {
        if(adapter.getCount() == visibleLastIndex && i == SCROLL_STATE_IDLE){
            new LoadDataThread().start();
        }
    }

    @Override
    public void onScroll(AbsListView absListView, int i, int i1, int i2) {
        visibleLastIndex = i + i1 - 1;
    }

    //刷新线程，刷新模拟数据
    class LoadDataThread extends Thread{
        @Override
        public void run() {
            //刷新成功后，发送消息给handler
            super.run();
            demoList.addAll(Arrays.asList("测试刷新消息1","测试刷新消息2","测试刷新消息3","测试刷新消息4","测试刷新消息5"));
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            handler.sendEmptyMessage(1);

        }
    }


    //list view适配器，设定每条消息。
    class Myadapter extends BaseAdapter{

        @Override
        public int getCount() {
            return demoList.size();
        }

        @Override
        public Object getItem(int i) {
            return demoList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = LayoutInflater.from(getActivity()).inflate(R.layout.share_msg_item,null);
            TextView tv = view.findViewById(R.id.tv_msg_item);
            tv.setText(demoList.get(i));
            return view;
        }
    }
}

