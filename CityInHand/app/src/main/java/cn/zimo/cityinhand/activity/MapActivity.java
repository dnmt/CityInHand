package cn.zimo.cityinhand.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.model.LatLng;
import com.jaeger.library.StatusBarUtil;

import cn.zimo.cityinhand.R;

public class MapActivity extends AppCompatActivity {

    // 百度地图视图容器
    private MapView mMapView;
    // 地图对象
    private BaiduMap mBaiduMap;

    // 定位相关
    private LocationClient mlocationClient;
    private MylocationListener mlistener;
    private Context context;

    private double mCurrentLatitude;
    private double mCurrentLongitude;
    private float mCurrentAccracy;

    // 回到当前位置
    private ImageView iv_back_to_my_loc;
    // 地图模式选择
    private ImageView iv_map_moshi;


    // 定位图层显示方式
    private MyLocationConfiguration.LocationMode locationMode;
    // 弹出式菜单
    private PopupMenu popup = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        SDKInitializer.initialize(getApplicationContext());
        SDKInitializer.setCoordType(CoordType.BD09LL);

        setContentView(R.layout.activity_map);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        StatusBarUtil.setTransparent(this);

        context = this;
        initViews();

        // 展示地图
        initLocation();

    }

    /**
     * 初始化布局
     */
    private void initViews() {

        mMapView = findViewById(R.id.watch_map);
        iv_back_to_my_loc = findViewById(R.id.iv_back_to_my_loc);
        iv_map_moshi = findViewById(R.id.iv_map_moshi);

        mBaiduMap = mMapView.getMap();
        // 开启交通图
        mBaiduMap.setTrafficEnabled(true);

        // 根据给定增量缩放地图级别
        MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(18.0f);
        mBaiduMap.setMapStatus(msu);
        MapStatus mMapStatus;//地图当前状态
        MapStatusUpdate mMapStatusUpdate;//地图将要变化成的状态
        mMapStatus = new MapStatus.Builder().overlook(-45).build();
        mMapStatusUpdate = MapStatusUpdateFactory.newMapStatus(mMapStatus);
        mBaiduMap.setMapStatus(mMapStatusUpdate);

    }

    /**
     * 定位方法
     */
    private void initLocation() {

        // 点击我的位置图标返回当前的位置
        iv_back_to_my_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToMyLocation();
            }
        });
        // 点击选择地图模式
        iv_map_moshi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPopupMenuClick(v);
            }
        });

        locationMode = MyLocationConfiguration.LocationMode.NORMAL;

        // 定位服务的客户端。宿主程序在客户端声明此类，并调用，目前只支持在主线程中启动
        mlocationClient = new LocationClient(this);
        mlistener = new MylocationListener();

        // 注册监听器
        mlocationClient.registerLocationListener(mlistener);
        // 配置定位SDK各配置参数，比如定位模式、定位时间间隔、坐标系类型等
        LocationClientOption option = new LocationClientOption();
        // 设置高精度定位
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
        // 设置坐标类型
        option.setCoorType("bd09ll");
        // 设置是否需要地址信息，默认为无地址
        option.setIsNeedAddress(true);
        // 设置是否打开gps进行定位
        option.setOpenGps(true);
        // 设置当GPS有效时，每秒钟输出一次结果
        option.setLocationNotify(true);
        // 设置直接可以获取结果集合list
        option.setIsNeedLocationPoiList(true);
        // 设置语义化输出结果
        option.setIsNeedLocationDescribe(true);
        // 设置扫描间隔，单位是毫秒，当<1000(1s)时，定时定位无效
        int span = 1*1000;
        option.setScanSpan(span);
        // 设置 LocationClientOption
        mlocationClient.setLocOption(option);

    }

    public void onPopupMenuClick(View v) {
        // 创建PopupMenu对象
        popup = new PopupMenu(this, v);
        // 将R.menu.menu_main菜单资源加载到popup菜单中
        getMenuInflater().inflate(R.menu.menu_map, popup.getMenu());
        // 为popup菜单的菜单项单击事件绑定事件监听器
        popup.setOnMenuItemClickListener(
                new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.id_map_common:
                                mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
                                break;
                            case R.id.id_map_site:
                                mBaiduMap.setMapType(BaiduMap.MAP_TYPE_SATELLITE);
                                break;
                            case R.id.id_map_traffic:
                                if (mBaiduMap.isTrafficEnabled()) {
                                    mBaiduMap.setTrafficEnabled(false);
                                    item.setTitle("实时交通(off)");
                                } else {
                                    mBaiduMap.setTrafficEnabled(true);
                                    item.setTitle("实时交通(on)");
                                }
                                break;
                            case R.id.id_map_mlocation:
                                backToMyLocation();
                                break;
                            case R.id.id_map_model_common:
                                //普通模式
                                locationMode = MyLocationConfiguration.LocationMode.NORMAL;
                                break;
                            case R.id.id_map_model_following:
                                //跟随模式
                                locationMode = MyLocationConfiguration.LocationMode.FOLLOWING;
                                break;
                            case R.id.id_map_model_compass:
                                //罗盘模式
                                locationMode = MyLocationConfiguration.LocationMode.COMPASS;
                                break;
                        }
                        return true;
                    }
                });
        popup.show();
    }


    @Override
    protected void onStart() {
        // 开启定位
        mBaiduMap.setMyLocationEnabled(true);
        if (!mlocationClient.isStarted()) {
            mlocationClient.start();
        }
        super.onStart();
    }

    @Override
    protected void onStop() {
        // 停止定位
        mBaiduMap.setMyLocationEnabled(false);
        mlocationClient.stop();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mMapView != null){
            mMapView.onDestroy();
        }
    }

    /**
     * 回到当前位置
     */
    public void backToMyLocation() {
        LatLng latLng = new LatLng(mCurrentLatitude, mCurrentLongitude);
        MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(latLng);
        mBaiduMap.setMapStatus(msu);

    }


    /**
     * 实现百度地图API定位的回调接口
     */
    public class MylocationListener implements BDLocationListener {
        private boolean isFirstIn = true;

        /**
         * 重写回调函数，在这里可以拿到定位信息
         * @param bdLocation
         */
        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            if (bdLocation.getLocType() == 167){
                Toast.makeText(context,"定位失败，错误代码：" + bdLocation.getLocType(),Toast.LENGTH_SHORT).show();
                return;
            }

            // BDLocation 回调的百度坐标类，内部封装了如经纬度、半径等属性信息
            // MyLocationData 定位数据,定位数据建造器
            /**
             * 可以通过BDLocation配置如下参数
             * 1.accuracy 定位精度
             * 2.latitude 百度纬度坐标
             * 3.longitude 百度经度坐标
             * 4.satellitesNum GPS定位时卫星数目 getSatelliteNumber() gps定位结果时，获取gps锁定用的卫星数
             * 5.speed GPS定位时速度 getSpeed()获取速度，仅gps定位结果时有速度信息，单位公里/小时，默认值0.0f
             * 6.direction GPS定位时方向角度
             * */

            mCurrentAccracy = bdLocation.getRadius();
            mCurrentLatitude = bdLocation.getLatitude();
            mCurrentLongitude = bdLocation.getLongitude();

            MyLocationData data = new MyLocationData.Builder()
                    .direction(bdLocation.getDirection())// 设定图标方向
                    .accuracy(mCurrentAccracy)// getRadius 获取定位精度,默认值0.0f
                    .latitude(mCurrentLatitude)// 百度纬度坐标
                    .longitude(mCurrentLongitude)// 百度经度坐标
                    .build();
            mBaiduMap.setMyLocationEnabled(true);
            // 设置定位数据, 只有先允许定位图层后设置数据才会生效，参见 setMyLocationEnabled(boolean)
            mBaiduMap.setMyLocationData(data);
            // 配置定位图层显示方式,三个参数的构造器
            /**
             * 1.定位图层显示模式
             * 2.是否允许显示方向信息
             * 3.用户自定义定位图标
             * */
            // 设置自定义图标
            MyLocationConfiguration configuration
                    = new MyLocationConfiguration(locationMode, true, null);
            // 设置定位图层配置信息，只有先允许定位图层后设置定位图层配置信息才会生效，参见 setMyLocationEnabled(boolean)
            mBaiduMap.setMyLocationConfigeration(configuration);
            // 判断是否为第一次定位,是的话需要定位到用户当前位置
            if (isFirstIn) {
                // 地理坐标基本数据结构
                LatLng latLng = new LatLng(bdLocation.getLatitude(), bdLocation.getLongitude());
                // 描述地图状态将要发生的变化,通过当前经纬度来使地图显示到该位置
                MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(latLng);
                // 改变地图状态
                mBaiduMap.setMapStatus(msu);
                if(bdLocation.getAddrStr() == null){
                    return;
                }
                isFirstIn = false;
                Toast.makeText(context,"当前的位置是：" + bdLocation.getAddrStr(),Toast.LENGTH_SHORT).show();
            }
        }
    }
}
