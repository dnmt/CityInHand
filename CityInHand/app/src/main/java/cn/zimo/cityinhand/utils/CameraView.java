package cn.zimo.cityinhand.utils;

import android.content.Context;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.os.Handler;

import java.io.IOException;
import java.util.List;
import java.util.jar.Attributes;

/**
 * Created by 方少 on 2018/6/27.
 * QQ：1378501215
 * Email：fangjunjiefjj@163.com
 */
public class CameraView extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder surfaceHolder;
    private Camera mcamera;

    public CameraView(Context context, AttributeSet attr){
        super(context,attr);
    }

    public void init(Camera camera){
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        mcamera = camera;
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if(mcamera == null){
            return;
        }
        try {
            mcamera.setDisplayOrientation(90);
            mcamera.setPreviewDisplay(surfaceHolder);
//            mcamera.autoFocus(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mcamera.startPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int i, int i1, int i2) {
        if(holder.getSurface() == null || mcamera == null)
            return;
        mcamera.stopPreview();
        try {
            mcamera.setDisplayOrientation(90);
            mcamera.setPreviewDisplay(surfaceHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mcamera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    public boolean handleMessage(Message message) {
        return false;
    }



}
