package cn.zimo.cityinhand.config;
/**
 * @author 子墨
 * @datetime 2018/7/25 16:15
 * @description 网络配置
 */
public class HttpConfig {

    // 服务器基础地址  "http://117.172.164.151:9999"
    public static String WEB_URL = "http://117.172.164.151:9999";
    // 文件服务器
    public static String FILE_URL = "http://117.172.164.151:9998" + "/api/spfile/upload";
    // 请求镇数据
    public static  String GET_TOWN = WEB_URL + "/ajax/grid/getTown";
    // 请求村数据
    public static  String GET_VILLAGE = WEB_URL + "/ajax/grid/getVillage?townId=";
    // 提交民情日记登记信息 "/ajax/save/copnote";
    public static  String POST_MIN_QING = WEB_URL + "/ajax/grid/saveCopnote";
    // 提交GIS事件登记信息
    public static  String POST_GIS = WEB_URL + "/ajax/grid/saveReportEvent";
    // 提交自行处理事件
    public static  String POST_SELF = WEB_URL + "/ajax/grid/saveReportEvent";
    // 编码
    public static  String ENCODE = "utf-8";
    // 文件上传路径
    public static  String FILE_UPLOAD = WEB_URL + "/api/spfile/upload";
    // 事件类别
    public static  String GET_EVENT = WEB_URL + "/ajax/pub/getevent-type?typeClass=1";
    // 实有房屋统计
    public static  String REAL_HOUSE_STATISTICS = WEB_URL + "/house/houseCount";
    // 出租房统计
    public static  String RENT_HOUSE_STATISTICS = WEB_URL + "/phone/rentHouseCount";
    // 实有房屋审核
    public static  String HOUSE_APPROVAL = WEB_URL + "/house/HouseApproval";
    // 居民房屋管理
    public static  String HOUSE_MANAGE = WEB_URL + "/house/peopleDetailManager";
    // 整体的SessionID
    public static String SessionId = null;
    // 签名
    public static String Sign = null;
    // 移动党建（ip有更改）
    public static String Dangjian = "http://117.172.164.151:9995";




}
