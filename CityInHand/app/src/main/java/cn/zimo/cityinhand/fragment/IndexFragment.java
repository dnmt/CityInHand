package cn.zimo.cityinhand.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

import cn.zimo.cityinhand.R;
import cn.zimo.cityinhand.activity.ChaJianActivity;
import cn.zimo.cityinhand.activity.LawActivity;
import cn.zimo.cityinhand.activity.MapActivity;
import cn.zimo.cityinhand.activity.MessageShareActivity;
import cn.zimo.cityinhand.activity.UserActivity;
import cn.zimo.cityinhand.activity.generalsearch.GeneralSearchActivity;
import cn.zimo.cityinhand.lawregulation.LawsRegulations;
import cn.zimo.cityinhand.mobilesignin.MobileSignIn;
import cn.zimo.cityinhand.officalnotice.OfficalNotice;
import cn.zimo.cityinhand.shareinformation.ShareInfo;


/**
 * @author 子墨
 * @datetime 2018/9/22 9:32
 * @description 主页碎片
 */
public class IndexFragment extends Fragment implements View.OnClickListener{

    private LinearLayout menu_contacts;// 联系人
    private LinearLayout menu_im;// 即时通讯
    private LinearLayout menu_fast;// 快捷方式
    private LinearLayout menu_common;// 通用功能
    private LinearLayout menu_official;// 公文通知
    private LinearLayout menu_share;// 信息共享
    private LinearLayout menu_attendance;// 移动考勤
    private LinearLayout menu_map;// 地图浏览
    private LinearLayout menu_law;// 法律法规
    private LinearLayout menu_supervice;// 公众监督
    private LinearLayout menu_search;// 通用查询
    private LinearLayout menu_plug_in;// 其他插件

    public IndexFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_index, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        menu_contacts = view.findViewById(R.id.menu_contacts);
        menu_im = view.findViewById(R.id.menu_im);
        menu_fast = view.findViewById(R.id.menu_fast);
        menu_common = view.findViewById(R.id.menu_common);
        menu_official = view.findViewById(R.id.menu_official);
        menu_share = view.findViewById(R.id.menu_share);
        menu_attendance = view.findViewById(R.id.menu_attendance);
        menu_map = view.findViewById(R.id.menu_map);
        menu_law = view.findViewById(R.id.menu_law);
        menu_supervice = view.findViewById(R.id.menu_supervice);
        menu_search = view.findViewById(R.id.menu_search);
        menu_plug_in = view.findViewById(R.id.menu_plug_in);
        setListeners();
    }

    private void setListeners() {
        menu_contacts.setOnClickListener(this);
        menu_im.setOnClickListener(this);
        menu_fast.setOnClickListener(this);
        menu_common.setOnClickListener(this);
        menu_official.setOnClickListener(this);
        menu_share.setOnClickListener(this);
        menu_attendance.setOnClickListener(this);
        menu_map.setOnClickListener(this);
        menu_law.setOnClickListener(this);
        menu_supervice.setOnClickListener(this);
        menu_search.setOnClickListener(this);
        menu_plug_in.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Context context = getActivity();
        Intent intent = new Intent();
        PackageManager packageManager = context.getPackageManager();
        switch (v.getId()){
            case R.id.menu_contacts:
                intent.setClassName("com.android.contacts","com.android.contacts.activities.PeopleActivity");
                break;
            case R.id.menu_im:
                intent.setClass(context, UserActivity.class);
                break;
            case R.id.menu_fast:
                intent = packageManager.getLaunchIntentForPackage("cn.zimo.smartcity");
                break;
            case R.id.menu_common:
                intent.setClassName("cn.zimo.smartcity","cn.zimo.smartcity.activity.sub0activity.ZongHeZhiHuiActivity");
                // 来试试密钥
//                intent = packageManager.getLaunchIntentForPackage("cn.zimo.smartcity");
                break;
            case R.id.menu_official:
                intent.setClass(getActivity(),OfficalNotice.class);
                break;
            case R.id.menu_share:
                intent.setClass(getActivity(),MessageShareActivity.class);
                break;
            case R.id.menu_attendance:
                intent.setClass(getActivity(),MobileSignIn.class);
                break;
            case R.id.menu_map:
                intent.setClass(context, MapActivity.class);
                break;
            case R.id.menu_law:
                intent.setClass(context,LawActivity.class);
                break;
            case R.id.menu_supervice:
                intent.setClassName("cn.zimo.smartcity","cn.zimo.smartcity.activity.sub0activity.YiDongWangGeActivity");
//                intent = packageManager.getLaunchIntentForPackage("cn.zimo.smartcity");
                break;
            case R.id.menu_search:
                intent.setClass(context,GeneralSearchActivity.class);
                break;
            case R.id.menu_plug_in:
//                Toast.makeText(context,"接口尚未开通，请等待",Toast.LENGTH_SHORT).show();
                intent.setClass(context,ChaJianActivity.class);
                break;
        }
        if (isIntentAvailable(context,intent)){
            startActivity(intent);
        }
    }

    public static boolean isIntentAvailable(Context context, Intent intent) {
        try{
            final PackageManager packageManager = context.getPackageManager();
            @SuppressLint("WrongConstant") List<ResolveInfo> list = packageManager.queryIntentActivities(intent,
                    PackageManager.GET_ACTIVITIES);
            return list.size() > 0;
        }catch (Exception e){
            Toast.makeText(context,"未安装智慧兴文",Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
