package cn.zimo.cityinhand.utils;

/**
 * Created by 方少 on 2018/6/27.
 * QQ：1378501215
 * Email：fangjunjiefjj@163.com
 */

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.StringDef;
import android.widget.TextView;



public class MDFontsUtils {
    public static Typeface OCTICONS;
    public static Typeface getOcticons(final Context context){
        if(OCTICONS == null){
            OCTICONS = getTypeface(context,"css/aui-iconfont.ttf");
        }
        return OCTICONS;
    }

    public static Typeface getTypeface(final Context context, final String name) {
        return Typeface.createFromAsset(context.getAssets(), name);
    }

    public static void setOcticons(final TextView... textViews) {
        if (textViews == null || textViews.length == 0)
            return;

        Typeface typeface = getOcticons(textViews[0].getContext());
        for (TextView textView : textViews)
            textView.setTypeface(typeface);
    }

}
