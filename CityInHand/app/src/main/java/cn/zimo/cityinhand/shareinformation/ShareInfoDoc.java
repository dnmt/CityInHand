package cn.zimo.cityinhand.shareinformation;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.zimo.cityinhand.R;
import cn.zimo.cityinhand.utils.FileChooseUtils;

/**
 * Created by 方少 on 2018/6/27.
 * QQ：1378501215
 * Email：fangjunjiefjj@163.com
 */
public class ShareInfoDoc extends Fragment {

    private ListView lv_share_doc;
    private List<File> fileList = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_share_docs,container,false);
        lv_share_doc = rootView.findViewById(R.id.listview_share_docs);
        //查找文件
        findFiles();
        lv_share_doc.setAdapter(new fileListAdapter());
        lv_share_doc.setOnItemClickListener(new  fileChoose());

        return rootView;
    }


    //扫描文件夹文件-----测试用
    private void findFiles(){
        if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File sdPath = Environment.getExternalStorageDirectory();
            File temp = new File(sdPath + "/lawDemoDoc");
            if(!temp.exists()){
                temp.mkdirs();
            }
            File[] files = temp.listFiles();
            for (File f : files) {
                String name = f.getName();
                String type = name.substring(name.indexOf(".") + 1,name.length());
                if(type.equals("doc") || type.equals("xls") || type.equals("pdf") || type.equals("docx")||type.equals("xlsx") ){
                    fileList.add(f);
                }
            }
        }else{
            Toast.makeText(getActivity(),"请检查sd卡",Toast.LENGTH_SHORT).show();
        }

    }

    class fileListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return fileList.size();
        }

        @Override
        public Object getItem(int i) {
            return fileList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = LayoutInflater.from(getActivity()).inflate(R.layout.file_list_item,null);
            TextView tv = view.findViewById(R.id.tv_fileName);
            ImageView img = view.findViewById(R.id.img_fileIcon);
            //TextView trash = view.findViewById(R.id.tv_trash);
            //trash.setText(MDFonts.Trash);
            //trash.setTextSize(20);
            //MDFontsUtils.setOcticons(trash);
            String name = fileList.get(i).getName();
            String type = name.substring(name.indexOf(".") + 1, name.length());
            if(type.equals("doc") || type.equals("docx")){
                img.setImageResource(R.drawable.word);
            }else if(type.equals("pdf")){
                img.setImageResource(R.drawable.pdf);
            }else if(type.equals("xls") || type.equals("xlsx")){
                img.setImageResource(R.drawable.xls);
            }
            tv.setText(name);

            return view;
        }
    }

    class fileChoose implements AdapterView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            // 点击预览
            File readFile = fileList.get(i);
            FileChooseUtils.openFileByPath(getActivity(),readFile.getAbsolutePath());
        }
    }
}
