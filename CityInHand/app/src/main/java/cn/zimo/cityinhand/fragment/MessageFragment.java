package cn.zimo.cityinhand.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import cn.zimo.cityinhand.R;
/**
 * @author 子墨
 * @datetime 2018/9/22 9:36
 * @description 消息碎片
 */
public class MessageFragment extends Fragment {

    private ListView lv_message_list;
    private Long currentTime = System.currentTimeMillis();
    private Random random = new Random();
    private String[] dateTime = {
            new Date(currentTime).toLocaleString(),
            new Date(currentTime + 1000*random.nextInt(10)).toLocaleString(),
            new Date(currentTime + 1000*(10+random.nextInt(10))).toLocaleString(),
            new Date(currentTime + 1000*(20+random.nextInt(10))).toLocaleString(),
            new Date(currentTime + 1000*(30+random.nextInt(10))).toLocaleString(),
            new Date(currentTime + 1000*(40+random.nextInt(10))).toLocaleString(),
            new Date(currentTime + 1000*(50+random.nextInt(10))).toLocaleString(),
            new Date(currentTime + 1000*(60+random.nextInt(10))).toLocaleString()};
    private String[] describeContent = {
            "欢迎来到掌上兴文APP",
            "在这里，我们为你准备了很多好用的功能",
            "我们提供公文通知、信息共享、移动考勤，地图浏览，法律法规，公众监督，通用查询等几大功能",
            "并且我们整合了相当多的插件，包括领导通，城管通，智慧兴文APP等",
            "尽管现在上线的功能并不多",
            "但请你耐心等待，更多功能正在开发中",
            "我们希望给你提供一个近乎完美的体验",
            "请开始你的使用把"
    };
    public MessageFragment() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_message, container, false);
    }

    private void init(){
        lv_message_list = getActivity().findViewById(R.id.lv_message_list);
        lv_message_list.setAdapter(new MessageAdapter());
    }
    //每条消息，的适配器
    class MessageAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return dateTime.length;
        }

        @Override
        public Object getItem(int i) {
            return dateTime[i];
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v = View.inflate(getActivity(),R.layout.messege_item,null);
            TextView tvd = v.findViewById(R.id.tv_message_describe);
            TextView tvt = v.findViewById(R.id.tv_message_timePoint);

            tvt.setText(dateTime[dateTime.length - i - 1]);
            tvd.setText(describeContent[describeContent.length - i - 1]);
            return v;
        }
    }
}
